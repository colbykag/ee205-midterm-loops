///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Midterm - Loops
///
/// @file    main.c
/// @version 1.0
///
/// Sums up values in arrays
///
/// Your task is to print out the sums of the 3 arrays...
///
/// The three arrays are held in a structure.  Consult numbers.h for the details.
///
/// For array1[], you'll iterate over using a for() loop.  The correct sum for 
/// array1[] is:  48723737032
/// 
/// For array2[], you'll iterate over it using a while()... loop.
///
/// For array3[], you'll iterate over it using a do ... while() loop.
///
/// Sample output:  
/// $ ./main
/// 11111111111
/// 22222222222
/// 33333333333
/// $
///
/// @brief Midterm - Loops - EE 205 - Spr 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "numbers.h"

int main() {

   long total1 = 0;
   // Sum array1[] with a for() loop and print the result
   int size_a = sizeof(threeArrays.array1)/sizeof(threeArrays.array1[0]);
   for(int i = 0; i < size_a; i++){
      total1 += threeArrays.array1[i];
   }
   printf("total for array1: %ld\n", total1);

   // Sum array2[] with a while() { } loop and print the result
   int j = 0;
   long total2 = 0;
   while(j < size_a)
   {
      total2 += threeArrays.array2[j];
      j++;
   }

   printf("total for array2: %ld\n", total2);

   // Sum array3[] with a do { } while() loop and print the result
   int k = 0;
   long total3 = 0;
   do {
      total3 += threeArrays.array3[k];
      k++;
   }
   while(k <= size_a);

   printf("total for array3: %ld\n", total3);
   long total = total1 + total2 + total3;
   printf("total for all three arrays: %ld\n", total);
	return 0;
}

